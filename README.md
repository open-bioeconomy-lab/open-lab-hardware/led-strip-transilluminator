# LED Strip Transilluminator

## Project Goal
To reduce the manufacturing time of a blue LED transilluminator by using LED strips rather than individually soldered components.

## Project Components

### LED Strips

1. SMD 5050 LED 470 nm strips, 60 LEDs per metre
12v and 14.4W per meter = 1.2 A per meter

2. SMD 3258 LED 470 nm strips, 120 LEDs per metre
12v and 9.6W per meter = 0.8 A per meter

### Power Source

 - [12V power supply, 2A](https://www.amazon.co.uk/gp/product/B07WN17HVX/ref=ppx_od_dt_b_asin_title_s00?ie=UTF8&psc=1)
 - Supplied with screw terminal barrel jack

### Aluminium Heatsink

 - [10 cm x 10 cm](https://futureeden.co.uk/collections/aluminium-heatsinks/products/aluminium-heatsink-t-series-profile-a-86mm-x-22mm)

### Case

 - [DT-2020 Plastic Project enclosure](https://www.altinkaya.eu/plastic-project-enclosures/116-dt-2020.html)
  - https://all-electronic-components.com/gb/aluminium-desk-box/207150-15x10x4-professional-quality-aluminium-enclosure-project-desk-box-for-electronic.html
  https://www.esr.co.uk/electronics/products/frame_enclosures.htm
https://evatron.com/basket/
https://www.metcase.co.uk/en/Metal-Enclosures/Unimet-Plus.htm